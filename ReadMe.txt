1. Do a docker pull of the Mysql container using the command: docker pull siyanda7/reg_db:latest
2. start the docker container by running: docker start reg_db
3. run the Mysql service, the user is root and password is dbpassword
4. extract the online_registration spring boot project
5. (Assuming Maven is installed)start run the spring project by using the command: mvn spring-boot:run
6. extract the online-registration Angular 8 project 
7. (Assuming the correct version of Angular CLI is installed) switch to the project directory and run the command: ng serve
8. You can then go to the browser and enter this url: http://localhost:4200/