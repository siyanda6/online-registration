package com.wanda.online_registration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;

@SpringBootApplication(scanBasePackages = {"com.wanda.online_registration"}, exclude = { SecurityAutoConfiguration.class })
@EntityScan(basePackages = {"com.wanda.online_registration"})
@EnableJpaRepositories(basePackages = {"com.wanda.online_registration"})
@EnableScheduling
public class OnlineRegistrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineRegistrationApplication.class, args);
	}

	@Bean
	@Scope("prototype")
	Logger logger(InjectionPoint injectionPoint) {
		if(injectionPoint != null && injectionPoint.getMember() != null) {
			return LoggerFactory.getILoggerFactory().getLogger(injectionPoint.getMember().getDeclaringClass().getName());
		}

		return LoggerFactory.getLogger(this.getClass().getSimpleName());
	}

	@Bean
	public TaskScheduler taskScheduler() {
		return new ConcurrentTaskScheduler();	//single threaded by default
	}

}
