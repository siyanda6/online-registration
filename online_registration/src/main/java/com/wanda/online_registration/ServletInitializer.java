package com.wanda.online_registration;

import com.wanda.online_registration.config.AppConfig;
import org.springframework.boot.Banner;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(OnlineRegistrationApplication.class, AppConfig.class).bannerMode(Banner.Mode.OFF);
	}

}
