package com.wanda.online_registration.config;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@ComponentScan(basePackages = {"com.wanda.online_registration.entities.*", "com.wanda.online_registration.mappers.*",
                                "com.wanda.online_registration.repositories.*"})
@Configuration
@Import({DatabaseConfig.class})
public class AppConfig {

    @Value("${spring.application.name}")
    private String applicationName;

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
