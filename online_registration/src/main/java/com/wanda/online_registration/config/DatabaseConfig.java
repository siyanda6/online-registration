package com.wanda.online_registration.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class DatabaseConfig extends EnvironmentalConfig {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private LocalContainerEntityManagerFactoryBean entityManagerBean;

    @Bean
    public DataSource dataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.url(getProperty("db.url"));
        dataSourceBuilder.driverClassName(getProperty("db.driver.class.name"));
        dataSourceBuilder.username(getProperty("db.username"));
        dataSourceBuilder.password(getProperty("db.password"));
        return dataSourceBuilder.build();
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean emFactory = new LocalContainerEntityManagerFactoryBean();
        emFactory.setDataSource(dataSource);
        emFactory.setPersistenceUnitName(getProperty("persistence-unit.name"));

        // Classpath scanning of @Component, @Service, etc annotated class
        emFactory.setPackagesToScan(
                "com.wanda.online_registration.entities",
                "com.wanda.online_registration.repositories",
                "com.wanda.online_registration.mappers"
        );

        // Vendor adapter
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        emFactory.setJpaVendorAdapter(vendorAdapter);

        // Hibernate properties
        Properties additionalProperties = new Properties();
        additionalProperties.put("hibernate.dialect", getProperty("hibernate.dialect"));
        additionalProperties.put("hibernate.show_sql", getProperty("hibernate.show_sql"));
        additionalProperties.put("spring.jpa.generate-ddl", getProperty("spring.jpa.generate-ddl"));
        additionalProperties.put("hibernate.hbm2dll.auto", getProperty("hibernate.hbm2dll.auto"));
        additionalProperties.put("hibernate.flushMode", getProperty("hibernate.flushMode"));
        additionalProperties.put("hibernate.order_inserts", getProperty("hibernate.order_inserts"));
        additionalProperties.put("hibernate.jdbc.batch_size", getProperty("hibernate.jdbc.batch_size"));
        additionalProperties.put("hibernate.orcer_updates", getProperty("hibernate.orcer_updates"));
        emFactory.setJpaProperties(additionalProperties);

        return emFactory;
    }

    @Bean
    public PlatformTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }
}
