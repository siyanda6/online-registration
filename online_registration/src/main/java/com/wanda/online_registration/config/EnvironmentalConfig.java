package com.wanda.online_registration.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class EnvironmentalConfig {

    @Autowired
    private Environment environment;

    String getProperty(final String keyName) {
        return environment.getProperty(keyName);
    }
}
