package com.wanda.online_registration.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableScheduling
@Slf4j
public class HttpClientConfig {

    // Indicates time period for keep-alive sessions
    private static final int DEFAULT_KEEP_ALIVE_TIME_MILLIS = 20 * 1_000;

    // Indicates max time delay before closing idle connections
    private static final int CLOSE_IDLE_CONNECTION_WAIT_TIME_SECS = 60 * 10;

    /**
     * Determines connection timeout (in millie seconds) until a connection is established.
     */
    @Value("${req.connection.timeout:10000}")
    private int connectionTimeout;

    /**
     * The timeout when requesting a connection from the connection manager.
     */
    @Value("${request.timeout:5000}")
    private int requestTimeout;

    /**
     * The timeout for waiting for data.
     */
    @Value("${req.socket.timeout:5000}")
    private int socketTimeout;

    /**
     * Application HTTP client connection pool size.
     */
    @Value("${application.httpclient.pool.size:10}")
    private int httpClientPoolSize;

    /**
     * Application HTTP client connection pool size.
     */
    @Value("${application.httpclient.max-per-root:100}")
    private int maxConnPerRoute;

    /**
     * Application HTTP client keepalive duration in seconds.
     */
    @Value("${application.httpclient.keepalive:120}")
    private int httpClientKeepAlive;

    @Bean
    public PoolingHttpClientConnectionManager poolingConnectionManager() {
        final Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .build();

        final PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(registry);
        connectionManager.setMaxTotal(httpClientPoolSize);
        connectionManager.setDefaultMaxPerRoute(maxConnPerRoute);
        return connectionManager;
    }

    /**
     * Creates HttpClient with supplied connection.
     *
     * @param poolingConnectionManager Indicates supplied Http pooling connection manager
     * @param keepAliveStrategy        Indicates keep-alive strategy
     * @return Constructed HttpClient
     */
    @Bean
    public CloseableHttpClient httpClient(PoolingHttpClientConnectionManager poolingConnectionManager,
                                          ConnectionKeepAliveStrategy keepAliveStrategy) {

        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(requestTimeout)
                .setConnectTimeout(connectionTimeout)
                .setSocketTimeout(socketTimeout)
                .build();

        return HttpClients.custom()
                .setDefaultRequestConfig(requestConfig)
                .setConnectionManager(poolingConnectionManager)
                .setKeepAliveStrategy(keepAliveStrategy)
                .build();
    }

    @Bean
    public ConnectionKeepAliveStrategy keepAliveStrategy() {
        return (response, context) -> {
            HeaderElementIterator iterator = new BasicHeaderElementIterator(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
            while (iterator.hasNext()) {
                HeaderElement headerElement = iterator.nextElement();
                String param = headerElement.getName();
                String value = headerElement.getValue();

                if (value != null && param.equalsIgnoreCase("timeout")) {
                    return Long.parseLong(value) * 1_000;
                }
            }
            return DEFAULT_KEEP_ALIVE_TIME_MILLIS;
        };
    }

    /**
     * Starts an idle connection monitor to continuously clean up stale connections.
     *
     * @param connectionManager created http client connection manager
     * @return Runnable task
     */
    @Bean
    public Runnable idleConnectionMonitor(final PoolingHttpClientConnectionManager connectionManager) {
        return new Runnable() {
            @Override
            @Scheduled(fixedDelay = 10_000)
            public void run() {
                try {
                    if (connectionManager != null) {
                        /*if (log.isTraceEnabled()) {
                            log.trace("run IdleConnectionMonitor - Closing expired and idle connections...");
                        }*/
                        connectionManager.closeExpiredConnections();
                        connectionManager.closeIdleConnections(CLOSE_IDLE_CONNECTION_WAIT_TIME_SECS, TimeUnit.SECONDS);
                    } else {
                        //log.trace("run IdleConnectionMonitor - Http Client Connection manager is not initialised");
                    }
                } catch (Exception e) {
                    //log.error("run IdleConnectionMonitor - Exception occurred. msg={}, e={}", e.getMessage(), e);
                }
            }
        };
    }
}
