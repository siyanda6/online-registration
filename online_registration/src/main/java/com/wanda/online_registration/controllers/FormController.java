package com.wanda.online_registration.controllers;

import com.wanda.online_registration.entities.Form;
import com.wanda.online_registration.pojos.FormPojo;
import com.wanda.online_registration.services.FormService;
import com.wanda.online_registration.validators.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "form", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class FormController {

    private final FormService formService;
    private final Validator validator;

    @Autowired
    public FormController(FormService formService, Validator validator) {

        this.formService = formService;
        this.validator = validator;
    }

    @CrossOrigin
    @GetMapping
    public ResponseEntity<List<FormPojo>> getAllForms() {

        try {
            List<FormPojo> forms = formService.getFormMapper().fromEntitiesToPojos(formService.getAllForms());

            if(CollectionUtils.isEmpty(forms)) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }

            return ResponseEntity.ok(forms);

        } catch (Exception ex) {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @CrossOrigin
    @PostMapping
    public ResponseEntity registerUser(@RequestBody FormPojo formPojo) {

        try {

            String errMsg = validator.validateFields(formPojo);

            if (StringUtils.hasText(errMsg)) {
                return ResponseEntity
                        .status(HttpStatus.FORBIDDEN)
                        .body(errMsg);
            }

            Form form = formService.getFormMapper().fromPojoToEntity(formPojo);
            formService.getFormMapper().fromEntityToPojo(formService.save(form));

            return ResponseEntity.ok().build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
