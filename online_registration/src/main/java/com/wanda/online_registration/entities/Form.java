package com.wanda.online_registration.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;

@Entity(name = "forms")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Form {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name ="full_name")
    private String fullName;

    @Column(name ="id_number")
    private String idNumber;

    @Column(name ="telephone_number")
    private String telephoneNumber;
}
