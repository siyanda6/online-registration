package com.wanda.online_registration.mappers;

import com.wanda.online_registration.entities.Form;
import com.wanda.online_registration.pojos.FormPojo;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {})
public interface FormMapper {

    FormPojo fromEntityToPojo(Form contribution);
    List<FormPojo> fromEntitiesToPojos(List<Form> contribution);

    Form fromPojoToEntity(FormPojo contributionPojo);
    List<Form> fromPojosToEntities(List<FormPojo> contributionPojos);
}
