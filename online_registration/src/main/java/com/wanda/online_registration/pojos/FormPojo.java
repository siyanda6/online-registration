package com.wanda.online_registration.pojos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class FormPojo implements Serializable {
    private static final long serialVersionUID = 9823736355278929L;

     private Long id;
     private String fullName;
     private String idNumber;
     private String telephoneNumber;
}
