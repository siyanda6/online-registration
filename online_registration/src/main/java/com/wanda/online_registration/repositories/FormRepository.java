package com.wanda.online_registration.repositories;

import com.wanda.online_registration.entities.Form;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FormRepository extends JpaRepository<Form, Long> {

    //Optional<User> findUserByUsername(String email);

}
