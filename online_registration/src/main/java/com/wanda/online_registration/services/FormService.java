package com.wanda.online_registration.services;

import com.wanda.online_registration.entities.Form;
import com.wanda.online_registration.mappers.FormMapper;
import com.wanda.online_registration.repositories.FormRepository;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FormService {

    private final Logger log;
    private final FormRepository formRepository;
    private FormMapper formMapper;

    @org.jetbrains.annotations.Contract(pure = true)
    @Autowired
    public FormService(Logger log,
                       FormRepository formRepository,
                       FormMapper formMapper) {
        this.log = log;
        this.formRepository = formRepository;
        this.formMapper = formMapper;
    }

    public List<Form> getAllForms() {
        return formRepository.findAll();
    }

    public Form save(Form form) {
        return formRepository.save(form);
    }

    public FormMapper getFormMapper() {
        return formMapper;
    }
}
