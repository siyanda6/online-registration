package com.wanda.online_registration.validators;

import com.wanda.online_registration.pojos.FormPojo;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class Validator {

    public String validateFields(FormPojo formPojo) {

        if(!StringUtils.hasText(formPojo.getFullName())){
            return "First Name is required";
        }

        if(!StringUtils.hasText(formPojo.getIdNumber())){
            return "Id Number is required";
        }

        if(!StringUtils.hasText(formPojo.getTelephoneNumber())){
            return "Telephone Number is required";
        }

        Pattern pattern = Pattern.compile("^\\d{13}$");
        Matcher matcher = pattern.matcher(formPojo.getIdNumber());

        if(!matcher.matches()){
            return "Please provide a valid ID Number";
        }

        pattern = Pattern.compile("^\\d{10}$");
        matcher = pattern.matcher(formPojo.getTelephoneNumber());

        if(!matcher.matches()){
            return "Please provide a valid Telephone Number";
        }

        return null;
    }
}
