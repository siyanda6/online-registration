package com.wanda.online_registration.services;

import com.wanda.online_registration.entities.Form;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServicesTest {

    @Autowired
    FormService formService;

    @Test
    public void saveFormTest() {

        try{
            Form form = new Form();
            form.setFullName("Test Tester");
            form.setIdNumber("1234567890123");
            form.setTelephoneNumber("1234567890");


            Form savedForm = formService.save(form);
            assert(savedForm != null);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Test
    public void getAllFormsTest() {

        List<Form> forms = formService.getAllForms();
        assert(forms.size() > 0);

    }
}
